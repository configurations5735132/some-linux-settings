# change system shell

```sh
chsh -s /path/to/shell # set system shell
chsh -s /bin/zsh # concrete example setting system shell
echo $SHELL # show wich shell is currently used as system shell
```
