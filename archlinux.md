

# crate pacman hooks

For creating a new hook just add a new file in to `/etc/pacman.d/hooks`

## hook for removing old cash

file content:

```
[Trigger]
Operation = Upgrade
Operation = Install
Operation = Remove
Type = Package
Target = *

[Action]
Description = Clean pacman cache
When = PostTransaction
Exec = /usr/bin/paccache -r
```

