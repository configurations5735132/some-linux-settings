# display hints when typing in password for sudo

**edit /etc/sudoers:**

```bash
sudoedit /etc/sudoers
```

**add the following line:**

`Defaults    env_reset,pwfeedback`


# set grub timeout to 0 while making it interruptible

1. In `etc/default/grub`, set `GRUB_TIMEOUT=0`
2. In the same file, set `GRUB_TIMEOUT_STYLE` to hidden or countdown. Don't set it to menu as it uses a different loop that is not interruptible.
3. In theory,pressing `SHIFT` before booting the system should interrupt the boot process and show the GRUB menu. In practice, on some EFI systems pressing `SHIFT` is not relayed to GRUB due to how the underlying firmware works. In those cases, press `SHIFT+non-ASCII` character (e.g. `SHIFT+F1`) before the boot process to get to the GRUB menu. Some times it also works with `ESC` (when pressing `ESC` you might end up in the grub shell. To exit it just type `normal` and press enter).


# change default applications (outside a desktop environment)

For changing default applications (e.g. default browser) without the help of a desktop environment
just edit `/etc/environment`


# Language and Time

## set language of "time" (data utility) (like names of months, days, ...)

data utility uses variable LC_TIME to know what language of days to use
- for temporary change:
```bash
LC_TIME=en_US.utf8
```

- for permanent change edit the value in `/etc/default/locale`
